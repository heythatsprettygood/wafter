package an.bri;

import an.bri.command.Commands;
import an.bri.event.EventListener;
import an.bri.utility.DataRegistry;
import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class Main extends JavaPlugin
{
    private final EventListener wafterListener = new EventListener();
    public static final DataRegistry registry = new DataRegistry();
    public static final HashMap<String, Boolean> commandSpies = new HashMap<String, Boolean>();
    private static Main instance;

    //create the command framework command manager
    private CommandsManager<CommandSender> commands;

    @Override
    public void onEnable()
    {
        instance = this;

        Bukkit.getServer().getPluginManager().registerEvents(wafterListener, this);
        this.setupCommands();
        this.setupFiles();

        for (Player p : Bukkit.getOnlinePlayers())
        {
            commandSpies.put(p.getName(), true);
        }
    }

    private void setupCommands() {
        this.commands = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender sender, String perm) {
                return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
            }
        };
        CommandsManagerRegistration cmdRegister = new CommandsManagerRegistration(this, this.commands);
        //Register your commands here
        cmdRegister.register(Commands.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String commandLabel, String[] args) {
        try {
            this.commands.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You don't have permission.");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
            } else {
                sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }

        return true;
    }

    public static Main getInstance()
    {
        return instance;
    }

    private void setupFiles()
    {
        DataRegistry.getPluginRegistryDirectory(this.getName());
    }
}
