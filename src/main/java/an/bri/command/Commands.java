package an.bri.command;

import an.bri.Main;
import an.bri.inventories.DankStashInventory;
import an.bri.utility.DataRegistry;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Arrays;

@SuppressWarnings({"RedundantThrows", "UnusedParameters"})
public class Commands
{
    @Command(aliases = {"stash"}, desc = "wanker")
    public static void stash(final CommandContext args, CommandSender sender) throws Exception
    {
        if (sender instanceof Player)
        {
            Player p = (Player) sender;
            p.openInventory(DankStashInventory.generate());
        }
    }

    @Command(aliases = {"fc", "forcechat"}, desc = "desk")
    public static void forcechat(final CommandContext args, CommandSender sender) throws Exception
    {
        if (args.argsLength() < 2)
        {
            sender.sendMessage("need more");
            return;
        }

        if (Bukkit.getPlayer(args.getString(0)) == null)
        {
            sender.sendMessage("need players pls");
            return;
        }

        Bukkit.getServer().getPlayer(args.getString(0)).chat(args.getJoinedStrings(1));
    }

    @Command(aliases = {"fountain"}, desc = "wewlad")
    public static void setFountainEntity(final CommandContext args, CommandSender sender) throws Exception
    {
        if (args.getString(0).equalsIgnoreCase("list"))
        {
            sender.sendMessage(Arrays.asList(EntityType.values()).toString());
        }
        else
        {
            DataRegistry.setRegistryValue(Main.getInstance().getName(), "fountain_entity", args.getString(0));
        }
    }
    
    @Command(aliases = {"commandspy", "cspy"}, desc = "don't worry about it m8")
    public static void commandSpy(final CommandContext args, CommandSender sender) throws Exception
    {
        if (!Main.commandSpies.containsKey(sender.getName()))
            Main.commandSpies.put(sender.getName(), false);
        else
            Main.commandSpies.put(sender.getName(), !Main.commandSpies.get(sender.getName()));
    }
}
