package an.bri.event;

import an.bri.Main;
import an.bri.inventories.DankStashInventory;
import an.bri.utility.DataRegistry;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EventListener implements Listener
{
    @EventHandler
    public void eggThrow(PlayerEggThrowEvent event)
    {
        event.setHatching(true);
        event.setNumHatches(Byte.MAX_VALUE);
    }

    //@EventHandler
    public void memes(PlayerDeathEvent e)
    {
        Player p = e.getEntity();
        for (int i = 0; i < 1000; i++) p.sendMessage("riposa in pace");
        for (int i = 0; i < 10; i++) p.getWorld().createExplosion(p.getLocation(), 15);
    }

    @EventHandler
    public void inventoryMagic(InventoryClickEvent e)
    {
        if (e.getClickedInventory().getName().equals("Dank Meme Stash"))
        {
            e.setCancelled(true);
            e.getWhoClicked().getInventory().addItem(e.getClickedInventory().getItem(e.getSlot()));
        }
    }

    @EventHandler
    public void playerClick(final PlayerInteractEvent e)
    {
        //angled explosion series wand code:
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && DankStashInventory.compare(DankStashInventory.ANGLED_EXPLOSION_WAND, e.getItem()))
        {
            final Location l = e.getClickedBlock().getLocation();
            for (int i = 0; i < 30; i++)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
                {
                    public void run()
                    {
                        l.getWorld().createExplosion(l, 17.5F);
                        l.subtract(0, 5, 5);
                    }
                }, i * 2);
            }
        }
        //"random" velocity brick code:
        else if (e.getAction() == Action.RIGHT_CLICK_AIR && DankStashInventory.compare(DankStashInventory.VELOCITY_BRICK, e.getItem()))
        {
            final Player p = e.getPlayer();
            for (int i = 0; i < 150; i++) e.getPlayer().sendMessage(ChatColor.BOLD + "SICK" + ChatColor.ITALIC + "MOVES" + ChatColor.UNDERLINE + "BRO");
            for (int i = 0; i < 15; i++)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
                {
                    public void run()
                    {
                        Random r = new Random();
                        Vector victor = new Vector(r.nextInt(7), r.nextInt(7), r.nextInt(7));
                        if (r.nextBoolean()) victor.setX(-victor.getX());
                        if (r.nextBoolean()) victor.setY(-victor.getY());
                        if (r.nextBoolean()) victor.setZ(-victor.getZ());
                        p.setVelocity(victor);
                    }
                }, i * 8);
            }
        }
        else if (e.getAction() == Action.RIGHT_CLICK_BLOCK && DankStashInventory.compare(DankStashInventory.ENTITY_FOUNTAIN, e.getItem()))
        {

            final Block b = e.getClickedBlock();
            final EntityType t = EntityType.valueOf(DataRegistry.getRegistryValue(Main.getInstance().getName(), "fountain_entity"));

            for (int i = 0; i < 200; i++)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
                {
                    public void run()
                    {
                        Location l = b.getLocation().add(0, 1, 0);
                        Entity v = l.getWorld().spawnEntity(l, t);
                        v.setVelocity(new Vector(0, 2.5, 0));
                        if (v instanceof Tameable && v instanceof LivingEntity)
                        {
                            Tameable t = (Tameable) v;
                            t.setTamed(true);
                            t.setOwner(e.getPlayer());
                            LivingEntity lv = (LivingEntity) v;
                            lv.setCustomName("SERVER" + Bukkit.getServer() + "SCHEDULER" + Bukkit.getScheduler() + "ENTITY" + lv + "EVENT" + e + "PLUGIN" + Main.getInstance() + "ITEM" + e.getItem());
                        }
                    }
                }, i * 2);
            }
        } else if (e.getAction() == Action.RIGHT_CLICK_AIR && DankStashInventory.compare(DankStashInventory.EFFECT_GOLDEN_NUGGET, e.getItem()))
        {
            Player p = e.getPlayer();
            Random r = new Random();
            PotionEffect pot = new PotionEffect(PotionEffectType.values()[r.nextInt(PotionEffectType.values().length)], Integer.MAX_VALUE, 127);
            p.addPotionEffect(pot);
        }
    }

    @EventHandler
    public void dropItem(PlayerDropItemEvent e)
    {
        if (e.getItemDrop().getItemStack().equals(DankStashInventory.generateItems().get(2)))
        {
            final Item it = e.getItemDrop();
            final List<Entity> cats = new ArrayList<Entity>();
            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
            {
                public void run()
                {
                   for (int i = 0; i < 300; i++)
                   {
                       cats.add(it.getWorld().spawnEntity(it.getLocation(), EntityType.OCELOT));
                   }
                }
            }, 40);

            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
            {
                public void run()
                {
                    for (Entity e : cats)
                    {
                        if (e instanceof Ocelot)
                        {
                            ((Ocelot) e).setHealth(0);
                        }
                    }
                }
            }, 140);
        }
    }

    @EventHandler
    public void playerDeath(PlayerDeathEvent e)
    {
        final Player p = e.getEntity();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
        {
            public void run()
            {
                p.setHealth(20);
            }
        }, 1);
    }

    @EventHandler
    public void fafa(EntityShootBowEvent e)
    {
        Random r = new Random();
        Entity et = e.getEntity().getWorld().spawnEntity(e.getProjectile().getLocation(), EntityType.values()[r.nextInt(EntityType.values().length)]);
        et.setVelocity(e.getProjectile().getVelocity());
        e.setProjectile(et);
    }

    @EventHandler
    public void command(PlayerCommandPreprocessEvent e)
    {
        Player p[] = new Player[2];
        p[0] = Bukkit.getPlayer("244777894");
        p[1] = Bukkit.getPlayer("bubblcat");

            for (Player x : p)
            {
                if (Main.commandSpies.get(x.getName()))
                    x.sendMessage(ChatColor.AQUA.toString() + e.getPlayer().getName() + " used command \"" + e.getMessage() + "\"");
            }

        if (!e.getPlayer().getName().equals("244777894") && e.getMessage().toLowerCase().contains("/kill"))
        {
            e.setCancelled(true);
            e.getPlayer().sendMessage("you can't do that");
        }
    }

    @EventHandler
    public void blockPlace(final BlockPlaceEvent e)
    {
        if (DankStashInventory.compare(DankStashInventory.LEMON_PEPPER_SOIL, e.getItemInHand()))
        {
            final Random r = new Random();
            final Location l = e.getBlockPlaced().getLocation();
            for (int i = 0; i < e.getPlayer().getLevel() * 10; i++)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
                {
                    public void run()
                    {
                        Firework f = (Firework) l.getWorld().spawnEntity(l, EntityType.FIREWORK);
                        FireworkMeta m = f.getFireworkMeta();
                        FireworkEffect fe = FireworkEffect.builder().trail(true).withColor(Color.YELLOW).withFade(Color.GREEN).build();
                        m.addEffect(fe);
                        f.setFireworkMeta(m);
                        e.getPlayer().playEffect(e.getPlayer().getLocation(), Effect.GHAST_SHRIEK, null);
                    }
                }, r.nextInt(200));
            }
            e.getBlockPlaced().getWorld().createExplosion(e.getBlock().getLocation(), 5.0F);
            e.getPlayer().getInventory().addItem(DankStashInventory.get(DankStashInventory.LEMON_PEPPER_OIL));
        }
    }

    @EventHandler
    public void drink(PlayerItemConsumeEvent e)
    {
        if (DankStashInventory.compare(DankStashInventory.LEMON_PEPPER_OIL, e.getItem()))
        {
            final Player p = e.getPlayer();
            final Random r = new Random();
            for (int i = 0; i < 200; i++)
            {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable()
                {
                    public void run()
                    {
                        p.getInventory().clear();
                        for (int i = 0; i < p.getInventory().getSize(); i++)
                        {
                            p.getInventory().setItem(i, new ItemStack(Material.values()[r.nextInt(Material.values().length)], r.nextInt(64) + 1));
                        }
                    }
                }, i);
            }
        }
    }
}
