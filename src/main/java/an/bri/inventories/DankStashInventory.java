package an.bri.inventories;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DankStashInventory
{
    public static final int ANGLED_EXPLOSION_WAND = 0;
    public static final int VELOCITY_BRICK = 1;
    public static final int OCELOT_BOOK = 2;
    public static final int ENTITY_FOUNTAIN = 3;
    public static final int EFFECT_GOLDEN_NUGGET = 4;
    public static final int LEMON_PEPPER_SOIL = 5;
    public static final int LEMON_PEPPER_OIL = 6;

    private static final List<ItemStack> items = generateItems();

    public static boolean compare(int id, ItemStack is)
    {
        return items.get(id).equals(is);
    }

    public static ItemStack get(int id)
    {
        return items.get(id);
    }

    private DankStashInventory()
    {}

    public static List<ItemStack> generateItems()
    {
        List<ItemStack> items = new ArrayList<ItemStack>();
        ItemStack is = new ItemStack(Material.BLAZE_ROD);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(ChatColor.GREEN + "a real man's dildo");
        List<String> lore = new ArrayList<String>();
        lore.add("pungus");
        lore.add(ChatColor.GOLD + "sugnup");
        im.setLore(lore);
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.CLAY_BRICK);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "D" + ChatColor.MAGIC + "i" + ChatColor.RESET + "CK B" + ChatColor.MAGIC + "ri" + ChatColor.RESET + "CK");
        lore.clear();
        lore.add("By inserting this barely-solid brick");
        lore.add("into one's urethra, one can show off");
        lore.add("some \"sick moves, bro\"");
        im.setLore(lore);
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.BOOK);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.RED + "oh no");
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.MAGMA_CREAM);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.DARK_PURPLE + "FOUNTAINTILATOR");
        lore.clear();
        lore.add(ChatColor.LIGHT_PURPLE + "villager");
        im.setLore(lore);
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.GOLD_NUGGET);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.ITALIC + "corn buttplug");
        lore.clear();
        lore.add(ChatColor.GREEN + "uh... mom... it's stuck in there");
        im.setLore(lore);
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.DIRT);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.YELLOW + "LEMON" + ChatColor.RESET + "_" + ChatColor.DARK_RED + "PEPPER" + ChatColor.RESET + "_" + ChatColor.DARK_GREEN + "SOIL");
        im.setLore(Collections.singletonList("maybe if you squuezed it hard enough you could get LEMON_PEPPER_OIL!"));
        is.setItemMeta(im);
        items.add(is);
        is = new ItemStack(Material.MILK_BUCKET);
        im = is.getItemMeta();
        im.setDisplayName(ChatColor.GRAY + "LEMON_PEPE_SOIL");
        is.setItemMeta(im);
        items.add(is);
        return items;
    }

    public static Inventory generate()
    {
        Inventory i = Bukkit.createInventory(null, 9, "Dank Meme Stash");
        for (ItemStack is : generateItems())
        {
            i.addItem(is);
        }
        return i;
    }
}
