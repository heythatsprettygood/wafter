package an.bri.utility;

import org.bukkit.Bukkit;

import java.io.File;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("SameParameterValue")
public class DataRegistry
{
    private static final HashMap<String, HashMap<String, List<String>>> registries = new HashMap<String, HashMap<String, List<String>>>();

    /**
     * reloads the plugin registry by reassigning values in its respective HashMap
     * @param pluginName the plugin whose registry will be reloaded
     */
    public static void reloadRegistry(String pluginName)
    {
        HashMap<String, List<String>> processingRegistry = new HashMap<String, List<String>>();
        File[] registryObjects = getPluginRegistryDirectory(pluginName).listFiles();
        if (registryObjects == null) return;
        for (File f : registryObjects)
        {
            processingRegistry.put(f.getName(), FileWrapper.readFile(f.getPath()));
        }
        registries.put(pluginName, processingRegistry);
    }

    /**
     * reloads a specific registry value instead of an entire
     * @param pluginName the plugin which the registry value belongs to
     * @param label the label of the value to be reloaded
     */
    private static void reloadRegistryValue(String pluginName, String label)
    {
        HashMap<String, List<String>> registry = registries.get(pluginName);
        if (registry == null)
        {
            registries.put(pluginName, new HashMap<String, List<String>>());
            reloadRegistryValue(pluginName, label);
            return;
        }

        File f = new File(getPluginRegistryDirectory(pluginName).getPath(), label);
        registry.put(label, FileWrapper.readFile(f.getAbsolutePath()));
        registries.put(pluginName, registry);
    }

    /**
     * sets the value of a registry item (will overwrite data if it already exists!)
     * will not reload the registry value
     * @param pluginName the plugin to assign the registry value to
     * @param label what to name the registry value
     * @param data the data to be filed under the label
     */
    public static void setRegistryValue(String pluginName, String label, String data)
    {
        File regFile = getRegistryFile(pluginName, label);
        FileWrapper.createFile(regFile.getPath(), true);
        FileWrapper.editFile(regFile.getPath(), false, data);
    }

    /**
     * gets the value of the registry item specified (this will reload the registry value before returning it)
     * @param pluginName the plugin which the registry value belongs to
     * @param label the label of the value to be returned
     * @return a list of strings (being the lines in the registry file)
     */
    public static String getRegistryValue(String pluginName, String label)
    {
        reloadRegistryValue(pluginName, label);
        return registries.get(pluginName).get(label).get(0);
    }

    /**
     * gets the specified plugin's registry directory, and creates it if it doesn't exist
     * @param pluginName plugin whose directory to find or create
     * @return the directory of the specified plugin's registry
     */
    public static File getPluginRegistryDirectory(String pluginName)
    {
        File f = new File(Bukkit.getPluginManager().getPlugin(pluginName).getDataFolder(), "registry");
        FileWrapper.mkdir(f.getAbsolutePath(), true);
        return f;
    }

    private static File getRegistryFile(String pluginName, String label)
    {
        return new File(getPluginRegistryDirectory(pluginName), label);
    }
}
