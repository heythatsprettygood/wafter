package an.bri.utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * A simple utility class for handling files and directories
 */
@SuppressWarnings({"SameParameterValue", "ResultOfMethodCallIgnored"})
class FileWrapper
{
    /**
     * Creates directory(s)
     * @param dir The path to the directory to create
     * @param useMkDirs True if you are creating more than 1 directory
     */
    public static void mkdir(String dir, boolean useMkDirs)
    {
        try
        {
            File f = new File(dir);
            if (!useMkDirs) f.mkdir();
            else f.mkdirs();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Creates a file in the given directory
     * @param path Path to where the file will be created
     * @param overwrite Whether to overwrite a file with the same name if one already exists
     */
    public static void createFile(String path, boolean overwrite)
    {
        try
        {
            File f = new File(path);
            if (f.exists() && overwrite)
                f.createNewFile();
            else if (f.exists() && !overwrite)
                System.out.println("File not created (would overwrite existing file)");
            else if (!f.exists())
                f.createNewFile();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Edits a file
     * @param path The path to the file to edit
     * @param appendMode Whether to clear the current text in the file and only have the provided, or to append to the current text
     * @param text String(s) that you would like to add to or replace the file text
     */
    public static void editFile(String path, boolean appendMode, String... text)
    {
        if (text.length == 0)
        {
            throw new IllegalArgumentException("Text must be supplied and text cannot be null");
        }

        try
        {
            File f = new File(path);
            PrintWriter writer = new PrintWriter(new FileWriter(f, appendMode));
            for (String s : text)
            {
                writer.append(s);
            }
            writer.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static long sizeOf(String path)
    {
        try
        {
            return Files.size(Paths.get(path)) / 1000;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Checks whether a file exists or not
     * @param path The path to the directory or file to check existence of
     * @return Whether the given file or directory exists
     */
    public static boolean fileExists(String path)
    {
        return Files.exists(Paths.get(path));
    }

    /**
     * Read the value of a file
     * @param path The path to the file to read
     * @return A list of String(s) which are the separate lines of the file
     */
    public static List<String> readFile(String path)
    {
        try
        {
            return Files.readAllLines(Paths.get(path), Charset.defaultCharset());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}